package players;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import battleshipitems.Constants;

public class PlayerComputerTest {

	PlayerComputer playerComputer;
	
	@Before
	public void setUp() {
		playerComputer = new PlayerComputer();
	}
	
	/**
	 * PlayerComputer has been created and the five ships have been randomly placed 
	 * on the players board.  Now must test to see that all the ships and the blocks
	 * they occupy are indeed added.
	 */
	@Test
	public void testRandomlyPlaceShip() {
		//get the computers board
		int [][] playersBoard = playerComputer.getplayersBoard();
		int countShipOne = 0;
		int countShipTwo = 0;
		int countShipThree = 0;
		int countShipFour = 0;
		int countShipFive = 0;
		//count how many blocks of each ship are placed on the board
		for (int x = 0; x < playersBoard.length; x++) {
			for (int y = 0; y < playersBoard.length; y++) {
				switch (playersBoard[x][y]) {
				case 1:
					countShipOne++;
					break;
				case 2:
					countShipTwo++;
					break;
				case 3:
					countShipThree++;
					break;
				case 4:
					countShipFour++;
					break;
				case 5:
					countShipFive++;
					break;
				}
			}
		}
		//we know how many blocks are ship should occupy
		assertEquals(2, countShipOne);
		assertEquals(3, countShipTwo);
		assertEquals(3, countShipThree);
		assertEquals(4, countShipFour);
		assertEquals(5, countShipFive);
	}

	/**
	 * Test that the setPlayersBoard() method changed an int[][].
	 * Create a new int[][], initialize it values to 7.  Then set the 
	 * players board to the new int[][].  Test if that works.
	 */
	@Test
	public void testSetPlayersBoard() {
		int[][] changePlayersBoard = new int[10][10];
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				changePlayersBoard[x][y] = 7;
			}
		}
		playerComputer.setPlayersBoard(changePlayersBoard);
		boolean notCorrect = false;
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				if (changePlayersBoard[x][y] != 7) {
					notCorrect = true;
				}
			}
		}
		assertFalse(notCorrect);
	}

	/**
	 * This is used when loading a saved game.  So create a int[][] of value 9's.
	 * Call the dropBombsFromReload method check to see if all the values in the 
	 * playersBoard were changed to Constants.BOMB_MARKE.
	 */
	@Test
	public void testDropBombsFromReload() {
		int [][] historyBombs = new int[10][10];
		for (int x = 0; x < historyBombs.length; x++) {
			for (int y = 0; y < historyBombs.length; y++) {
				historyBombs[x][y] = Constants.BOMB_MARKER;
			}
		}
		boolean dropped = true;
		playerComputer.dropBombsFromReload(historyBombs);
		int [][] playersBoard = playerComputer.getplayersBoard();
		for (int x = 0; x < playersBoard.length; x++) {
			for (int y = 0; y < playersBoard.length; y++) {
				if (playersBoard[x][y] != Constants.BOMB_MARKER) {
					dropped = false;
				}
			}
		}
		assertTrue(dropped);
	}

	/**
	 * Check that the bombs are getting dropped and that the method dropping the
	 * bombs are returning the correct responses.
	 */
	@Test
	public void testDropOpponentsBomb() {
		//change players board so all values are 0
		int[][] changePlayersBoard = new int[10][10];
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				changePlayersBoard[x][y] = 0;
			}
		}
		playerComputer.setPlayersBoard(changePlayersBoard);
		//now know that all values are 0, test illegal move
		char status = playerComputer.dropOpponentsBomb(21, 12);
		boolean success = false;
		if (status == Constants.ILLEGAL_POSITION) {
			success = true;
		}
		assertTrue(success);
		success = false;
		//test for a miss
		status = playerComputer.dropOpponentsBomb(0, 0);
		if (status == Constants.MISSED) {
			success = true;
		}
		assertTrue(success);
		success = false;
		//test for a hit, must mark the board with a ship first
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				changePlayersBoard[x][y] = 1;
			}
		}
		playerComputer.setPlayersBoard(changePlayersBoard);
		status = playerComputer.dropOpponentsBomb(1, 1);
		if (status == Constants.HIT_SHIP) {
			success = true;
		}
		assertTrue(success);
		success = false;
		//test for already bombed
		status = playerComputer.dropOpponentsBomb(1, 1);
		if (status == Constants.ALREADY_BOMBED) {
			success = true;
		}
		assertTrue(success);
	}

	/**
	 * Test that the players looses if there are no more ships left.
	 */
	@Test
	public void testHasPlayerLost() {
		int[][] changePlayersBoard = new int[10][10];
		//test one more ship block left
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				if ((x == 0) && (y == 0)) {
					changePlayersBoard[0][0] = 1;
				} else {
					changePlayersBoard[x][y] = 9;
				}
			}
		}
		playerComputer.setPlayersBoard(changePlayersBoard);
		boolean hasPlayerLost = playerComputer.hasPlayerLost();
		assertFalse(hasPlayerLost);
		//all ships sunk
		changePlayersBoard[0][0] = 9;
		playerComputer.setPlayersBoard(changePlayersBoard);
		hasPlayerLost = playerComputer.hasPlayerLost();
		assertTrue(hasPlayerLost);
	}

}
