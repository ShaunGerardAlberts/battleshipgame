package players;

import battleshipitems.Constants;

/**
 * The human player will have the option for their ships to be automatically placed,
 * or the can manually place them.
 * 
 * @author Shaun ALberts
 *
 */

public class PlayerHuman extends Player {	
	//extra fields so human can assign their name
	private String playerName;
	
	/**
	 * Call the super() and set the players name.
	 * @param playerName
	 */
	public PlayerHuman(String playerName) {
		super();
		this.playerName = playerName;
	}

	/**
	 * Give the user the option to randomly place the ships for them.
	 * 
	 */
	public void rondomPlacePieces() {
		this.randomlyPlaceShip(smallShip);
		this.randomlyPlaceShip(mediumShipOne);
		this.randomlyPlaceShip(mediumShipTwo);
		this.randomlyPlaceShip(largeShip);
		this.randomlyPlaceShip(xlShip);
	}
	

	/**
 	 * Let the user place a ship on the board.  Is going to be very tedious using the text UI.  So
	 * ask the user if they would prefer the computer to place it for them.
	 * 
	 * @param shipIndicator
	 * @param posX
	 * @param posY
	 * @param placeVerticalOrHorizontal
	 * @return boolean
	 */
	public boolean explicitlyPlaceShip(char shipIndicator, int posX, int posY, char placeVerticalOrHorizontal) {
		boolean placed;
		if (shipIndicator == Constants.SMALL_SHIP) {
			placed = playersBoard.placeShip(posX, posY, smallShip, placeVerticalOrHorizontal);
		} else if (shipIndicator == Constants.MEDIUM_SHIP_ONE) {
			placed = playersBoard.placeShip(posX, posY, mediumShipOne, placeVerticalOrHorizontal);
		} else if (shipIndicator == Constants.MEDIUM_SHIP_TWO) {
			placed = playersBoard.placeShip(posX, posY, mediumShipTwo, placeVerticalOrHorizontal);
		} else if (shipIndicator == Constants.LARGE_SHIP) {
			placed = playersBoard.placeShip(posX, posY, largeShip, placeVerticalOrHorizontal);
		} else {//if (shipIndicator == Constants.X_LARGE_SHIP) {
			placed = playersBoard.placeShip(posX, posY, xlShip, placeVerticalOrHorizontal);
		}
		return placed;
	}
	
	/**
	 * Return the players name.  Is useful when saving and loading game.
	 * @return String
	 */
	public String getPlayerName() {
		return this.playerName;
	}
}
