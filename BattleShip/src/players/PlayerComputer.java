package players;
/**
 * With the computer players the ships are automatically placed.
 * 
 * @author Shaun Alberts
 *
 */
public class PlayerComputer extends Player {

	public PlayerComputer() {
		super();
		// for the computer, the ships must all be placed automatically
		this.randomlyPlaceShip(smallShip);
		this.randomlyPlaceShip(mediumShipOne);
		this.randomlyPlaceShip(mediumShipTwo);
		this.randomlyPlaceShip(largeShip);
		this.randomlyPlaceShip(xlShip);
	}
}
