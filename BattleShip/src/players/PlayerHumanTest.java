package players;

import static org.junit.Assert.*;

import org.junit.Test;

import battleshipitems.Constants;

public class PlayerHumanTest {

	PlayerHuman playerHuman;
	
	@Test
	public void testRondomPlacePieces() {
		playerHuman = new PlayerHuman("Testing");
		playerHuman.rondomPlacePieces();
		//get the computers board
		int [][] playersBoard = playerHuman.getplayersBoard();
		int countShipOne = 0;
		int countShipTwo = 0;
		int countShipThree = 0;
		int countShipFour = 0;
		int countShipFive = 0;
		//count how many blocks of each ship are placed on the board
		for (int x = 0; x < playersBoard.length; x++) {
			for (int y = 0; y < playersBoard.length; y++) {
				switch (playersBoard[x][y]) {
				case 1:
					countShipOne++;
					break;
				case 2:
					countShipTwo++;
					break;
				case 3:
					countShipThree++;
					break;
				case 4:
					countShipFour++;
					break;
				case 5:
					countShipFive++;
					break;
				}
			}
		}
		//we know how many blocks are ship should occupy
		assertEquals(2, countShipOne);
		assertEquals(3, countShipTwo);
		assertEquals(3, countShipThree);
		assertEquals(4, countShipFour);
		assertEquals(5, countShipFive);
	}
	
	@Test
	public void testExplicitlyPlaceShip() {
		playerHuman = new PlayerHuman("Testing");
		playerHuman.explicitlyPlaceShip(Constants.SMALL_SHIP, 0, 0, Constants.VERTICAL);
		boolean placed = true;
		int [][] playersBoard = playerHuman.getplayersBoard();
		for (int x = 0; x < playersBoard.length; x++) {
			for (int y = 0; y < playersBoard.length; y++) {
				if (x == 0 && y == 0) {
					if (playersBoard[x][y] != 1) {
						placed = false;
					}
				} else if (x==1 && y==0) {
					if (playersBoard[x][y] != 1) {
						placed = false;
					}
				}
				
			}
		}
		assertTrue(placed);
	}

	/**
	 * Test that the setPlayersBoard() method changed an int[][].
	 * Create a new int[][], initialize it values to 7.  Then set the 
	 * players board to the new int[][].  Test if that works.
	 */
	@Test
	public void testSetPlayersBoard() {
		playerHuman = new PlayerHuman("Testing");
		int[][] changePlayersBoard = new int[10][10];
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				changePlayersBoard[x][y] = 7;
			}
		}
		playerHuman.setPlayersBoard(changePlayersBoard);
		boolean notCorrect = false;
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				if (changePlayersBoard[x][y] != 7) {
					notCorrect = true;
				}
			}
		}
		assertFalse(notCorrect);
	}
	
	/**
	 * This is used when loading a saved game.  So create a int[][] of value 9's.
	 * Call the dropBombsFromReload method check to see if all the values in the 
	 * playersBoard were changed to Constants.BOMB_MARKE.
	 */
	@Test
	public void testDropBombsFromReload() {
		playerHuman = new PlayerHuman("Testing");
		int [][] historyBombs = new int[10][10];
		for (int x = 0; x < historyBombs.length; x++) {
			for (int y = 0; y < historyBombs.length; y++) {
				historyBombs[x][y] = Constants.BOMB_MARKER;
			}
		}
		boolean dropped = true;
		playerHuman.dropBombsFromReload(historyBombs);
		int [][] playersBoard = playerHuman.getplayersBoard();
		for (int x = 0; x < playersBoard.length; x++) {
			for (int y = 0; y < playersBoard.length; y++) {
				if (playersBoard[x][y] != Constants.BOMB_MARKER) {
					dropped = false;
				}
			}
		}
		assertTrue(dropped);
	}
	
	/**
	 * Check that the bombs are getting dropped and that the method dropping the
	 * bombs are returning the correct responses.
	 */
	@Test
	public void testDropOpponentsBomb() {
		playerHuman = new PlayerHuman("Testing");
		//change players board so all values are 0
		int[][] changePlayersBoard = new int[10][10];
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				changePlayersBoard[x][y] = 0;
			}
		}
		playerHuman.setPlayersBoard(changePlayersBoard);
		//now know that all values are 0, test illegal move
		char status = playerHuman.dropOpponentsBomb(21, 12);
		boolean success = false;
		if (status == Constants.ILLEGAL_POSITION) {
			success = true;
		}
		assertTrue(success);
		success = false;
		//test for a miss
		status = playerHuman.dropOpponentsBomb(0, 0);
		if (status == Constants.MISSED) {
			success = true;
		}
		assertTrue(success);
		success = false;
		//test for a hit, must mark the board with a ship first
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				changePlayersBoard[x][y] = 1;
			}
		}
		playerHuman.setPlayersBoard(changePlayersBoard);
		status = playerHuman.dropOpponentsBomb(1, 1);
		if (status == Constants.HIT_SHIP) {
			success = true;
		}
		assertTrue(success);
		success = false;
		//test for already bombed
		status = playerHuman.dropOpponentsBomb(1, 1);
		if (status == Constants.ALREADY_BOMBED) {
			success = true;
		}
		assertTrue(success);
	}

	/**
	 * Test that the players looses if there are no more ships left.
	 */
	@Test
	public void testHasPlayerLost() {
		playerHuman = new PlayerHuman("Testing");
		int[][] changePlayersBoard = new int[10][10];
		//test one more ship block left
		for (int x = 0; x < changePlayersBoard.length; x++) {
			for (int y = 0; y < changePlayersBoard.length; y++) {
				if ((x == 0) && (y == 0)) {
					changePlayersBoard[0][0] = 1;
				} else {
					changePlayersBoard[x][y] = 9;
				}
			}
		}
		playerHuman.setPlayersBoard(changePlayersBoard);
		boolean hasPlayerLost = playerHuman.hasPlayerLost();
		assertFalse(hasPlayerLost);
		//all ships sunk
		changePlayersBoard[0][0] = 9;
		playerHuman.setPlayersBoard(changePlayersBoard);
		hasPlayerLost = playerHuman.hasPlayerLost();
		assertTrue(hasPlayerLost);
	}
}















