package players;

/**
 * Create a Player superclass, that can be inherited by human and computer classes.
 * Each player has a Board.
 * 
 * @author Shaun Alberts
 *
 */

import java.util.Random;

import battleshipitems.Board;
import battleshipitems.Constants;
import battleshipitems.Ship;


public abstract class Player {

	protected Board playersBoard;

	protected Ship smallShip = new Ship(1, 2);
	protected Ship mediumShipOne = new Ship(2,3);
	protected Ship mediumShipTwo = new Ship(3,3);
	protected Ship largeShip = new Ship(4,4);
	protected Ship xlShip = new Ship(5,5);

	public Player() {
		// first we need a board
		playersBoard = new Board();//all values of board default to 0
	}

	/**
	 * This is to randomly place a ship. We will need a random X and Y coordinate, and a
	 * random direction, either Vertical or Horizontal. Then the passed ship can
	 * be placed.  
	 * 
	 * I'm going to give the player the option to let the computer randomly
	 * place the ship for them, therefore I've included this in the superclass.  Typically, 
	 * or rather when using the GUI you wouldn't want to do that.  Then it would make sense to 
	 * put this method only in the computer players subclass.
	 * 
	 * @return boolean value
	 */
	protected boolean randomlyPlaceShip(Ship ship) {
		Random rand = new Random();
		// get random direction, going to use a integer value, 0 = vertical, 1 = horizontal
		int randDirNum = rand.nextInt(2);
		// set the direction depending on the random number
		char randDir;
		if (randDirNum == 0) {
			randDir = Constants.VERTICAL;
		} else {
			randDir = Constants.HORITONTAL;
		}
		// try place ship, may be invalid so repeat until validly placed
		boolean placed;
		do {
			// random coordinates(0-9)
			int randomX = rand.nextInt(10);
			int randomY = rand.nextInt(10);
			//try place the board, if goes out of bounds or already occupied can't place
			placed = playersBoard.placeShip(randomX, randomY, ship, randDir);
		} while (!placed);
		return true;
	}
	
	/**
	 * To alter the board once it has been created and defaulted to 0 values.
	 * This is used when loading the game.
	 * 
	 * @param setBoard
	 */
	public void setPlayersBoard(int[][] setBoard) {
		playersBoard.setBoardArray(setBoard);
	}
	
	/**
	 * Takes a array[][] of past dropped bombs, and places all those bombs 
	 * on the opponents board.
	 * 
	 * Specifically for when the game is being loaded from a saved game.
	 * Now the bombs to a point so can't randomly place them.
	 * 
	 * When dropping bombs pay attention.  The historyBoard must come from 
	 * the opponent.
	 * 
	 * @param historyBoard
	 */
	public void dropBombsFromReload(int[][] historyBoard) {
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				//only drop of value was 9, indicating a dropped bomb
				if (historyBoard[x][y] == 9) {
					//got the coordinate to drop bomb
					this.dropOpponentsBomb(x, y);
				}
			}
		}
	}

	/**
	 * Returns the players board, showing where the ships are and what has been 
	 * bombed.
	 * 
	 * @return int[][]
	 */
	public int[][] getplayersBoard() {
		return playersBoard.getBoardArray();
	}

	/**
	 * Coordinates must come from the opponent, and if valid position must be dropped.
	 * The ship that gets hit must be affected.
	 * 
	 * @param posX
	 * @param posY
	 * @return char - success of the dropped bomb
	 */
	public char dropOpponentsBomb(int posX, int posY) {
		//using the playersBoard drop the bomb, get the status of the dropped bomb
		char bombStatus = playersBoard.dropBomb(posX, posY);
		
		//the bombStatus give a lot of information, return it to the caller
		if (bombStatus == Constants.MISSED) {
			return Constants.MISSED;
		} else if (bombStatus == Constants.ILLEGAL_POSITION) {
			return Constants.ILLEGAL_POSITION;
		} else if (bombStatus == Constants.ERROR) {//for test purposes
			return Constants.ERROR;
		} else if(bombStatus == Constants.ALREADY_BOMBED) {
			return Constants.ALREADY_BOMBED;
		} else {// hit something
			// now decrease the blocks of the ship that got hit, and see if its sunk
			boolean shipSunk = isShipSunk(bombStatus);
			if (shipSunk) {
//				System.out.println("Ship has been sunk");
				return Constants.HIT_SHIP_AND_SUNK;
			}
//			System.out.println("You hit something!!");
			return Constants.HIT_SHIP;
		}
	}

	/**
	 * Test if all your ships have been sunk. If so you have lost the game.
	 * 
	 * @return boolean
	 */
	public boolean hasPlayerLost() {
		int [][] board = playersBoard.getBoardArray();
		return playersBoard.isGameOver(board);
	}

	/**
	 * This will decrease the block left for a ship and return weather the ship
	 * is sunk or not. If the blocks left are 0 the ship is sunken.
	 * 
	 * @param bombStatus
	 * @return Boolean
	 */
	private boolean isShipSunk(char bombStatus) {
		//decrease the blocks of the ship that has been hit.
		if (bombStatus == Constants.SMALL_SHIP) {
			smallShip.decreaseBlocksLeft();
			if (smallShip.getIsSunk()) {
				return true;
			}
		} else if (bombStatus == Constants.MEDIUM_SHIP_ONE) {
			mediumShipOne.decreaseBlocksLeft();
			if (mediumShipOne.getIsSunk()) {
				return true;
			}
		} else if (bombStatus == Constants.MEDIUM_SHIP_TWO) {
			mediumShipTwo.decreaseBlocksLeft();
			if (mediumShipTwo.getIsSunk()) {
				return true;
			}
		} else if (bombStatus == Constants.LARGE_SHIP) {
			largeShip.decreaseBlocksLeft();
			if (largeShip.getIsSunk()) {
				return true;
			}
		} else if (bombStatus == Constants.X_LARGE_SHIP) {
			xlShip.decreaseBlocksLeft();
			if (xlShip.getIsSunk()) {
				return true;
			}
		} else {
			// error, shouldn't be able to get here
		}
		return false;
	}
	
	/**
	 * Get the number of blocks left for each ship.
	 * 
	 * @return int[] - blocks left of players' ships.
	 */
	public int[] getPlayerBlocksLeft() {
		int[] blocksLeft = new int[5];
		blocksLeft[0] = smallShip.getBlocksLeft();
		blocksLeft[1] = mediumShipOne.getBlocksLeft();
		blocksLeft[2] = mediumShipTwo.getBlocksLeft();
		blocksLeft[3] = largeShip.getBlocksLeft();
		blocksLeft[4] = xlShip.getBlocksLeft();
		return blocksLeft;
	}
	
	/**
	 * Set the blocks left of the ship.
	 * 
	 * @param blocksLeft
	 */
	public void setShipOneBlocks(int blocksLeft) {
		smallShip.setShipBlocks(blocksLeft);
	}
	
	/**
	 * Set the blocks left of the ship.
	 * 
	 * @param blocksLeft
	 */
	public void setShipTwoBlocks(int blocksLeft) {
		mediumShipOne.setShipBlocks(blocksLeft);
	}
	
	/**
	 * Set the blocks left of the ship.
	 * 
	 * @param blocksLeft
	 */
	public void setShipThreeBlocks(int blocksLeft) {
		mediumShipTwo.setShipBlocks(blocksLeft);
	}
	
	/**
	 * Set the blocks left of the ship.
	 * 
	 * @param blocksLeft
	 */
	public void setShipFourBlocks(int blocksLeft) {
		largeShip.setShipBlocks(blocksLeft);
	}
	
	/**
	 * Set the blocks left of the ship.
	 * @param blocksLeft
	 */
	public void setShipFiveBlocks(int blocksLeft) {
		xlShip.setShipBlocks(blocksLeft);
	}

}
