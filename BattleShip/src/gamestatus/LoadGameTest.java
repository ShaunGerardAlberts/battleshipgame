package gamestatus;
/**
 * Test the LoadGame class.  A savedGame.txt file must exist.  Will chack to ensure that 
 * the file contanins the correct contents.
 */
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class LoadGameTest {

	LoadGame loadGame;
	String [] readValues = new String[5];
	
	
	@Before
	public void setUp() throws Exception {
		loadGame = new LoadGame();
	}

	/** 
	 * Can the file be found and read.
	 */
	@Test
	public void testReadFile() {
		boolean readFile = false;
		try {
			loadGame.readFile();
			readFile = true;
		} catch (IOException ioe) {
			fail("File cannot be read.  Ensure it exists.");
		}
		assertTrue(readFile);
	}

	/**
	 * Can the 1st value in the file be cast to a string value.
	 */
	@Test
	public void testGetPlayersName() {
		this.readFile();
		boolean read = false;
		try {
			@SuppressWarnings("unused")
			String name = readValues[0];
			read = true;
		} catch (Exception ex) {
			fail("Name must be able to be cast to a string value");
		}
		assertTrue(read);
	}

	/**
	 * Is the 2nd value 'humansTurn' or 'computersTurn'
	 */
	@Test
	public void testIsHumansTurn() {
		this.readFile();
		boolean passedTest = false;
		if (readValues[1].equals("humansTurn") || readValues[1].equals("computersTurn")) {
			passedTest = true;
		}
		assertTrue(passedTest);
	}

	/**
	 * Can the 3rd value be split on ','
	 */
	@Test
	public void testGetInitialHumansArray() {
		this.readFile();
		@SuppressWarnings("unused")
		String [] initialHumanArray = new String[100];
		boolean passedTest = true;
		try {
			initialHumanArray = readValues[2].split(",");
			passedTest = true;
		} catch (Exception ex) {
			fail("Must be able to cast to array[]");
		}
		assertTrue(passedTest);
	}

	/**
	 * Can the 4th value be split on ','
	 */
	@Test
	public void testGetInitialComputerArray() {
		this.readFile();
		@SuppressWarnings("unused")
		String [] initialComputersArray = new String[100];
		boolean passedTest = true;
		try {
			initialComputersArray = readValues[3].split(",");
			passedTest = true;
		} catch (Exception ex) {
			fail("Must be able to cast to array[]");
		}
		assertTrue(passedTest);
	}

	/**
	 * Can the 5th value be split on ','
	 */
	@Test
	public void testGetHumansHistoryPicks() {
		this.readFile();
		@SuppressWarnings("unused")
		String [] humanHistoryPicks = new String[100];
		boolean passedTest = true;
		try {
			humanHistoryPicks = readValues[4].split(",");
			passedTest = true;
		} catch (Exception ex) {
			fail("Must be able to cast to array[]");
		}
		assertTrue(passedTest);
	}

	/**
	 * Can the 6th value be split on ','
	 */
	@Test
	public void testGetComputersHistoryPicks() {
		this.readFile();
		@SuppressWarnings("unused")
		String [] computerHistoryPicks = new String[100];
		boolean passedTest = true;
		try {
			computerHistoryPicks = readValues[5].split(",");
			passedTest = true;
		} catch (Exception ex) {
			fail("Must be able to cast to array[]");
		}
		assertTrue(passedTest);
	}
	
	/**
	 * Test the blocks left for both players
	 */
	@Test
	public void testBlocksLeft() {
		this.readFile();
		String[] getBlocksLeft = {readValues[6], readValues[7], readValues[8], readValues[9],
				readValues[10], readValues[11], readValues[12], readValues[13], readValues[14],
				readValues[15]};
		//
		boolean validBlocksLeft = true;
		for (String strBlockLeft : getBlocksLeft) {
			try {
				int blocksLeft = Integer.parseInt(strBlockLeft);
				//we know that the blocks can't be less than 0 and greater than 5
				if (blocksLeft < 0 || blocksLeft > 5) {
					validBlocksLeft = false;
				}
			} catch (Exception ex) {
				validBlocksLeft = false;
			}
		}
		assertTrue(validBlocksLeft);
	}
	
	/**
	 * Gets values of savedGame.txt and splits on ';' to get an array of 6 values.
	 * @return
	 * @throws IOException
	 */
	private void readFile() {
		try {
		BufferedReader br = new BufferedReader(new FileReader("savedGame.txt"));
			String currentLine;
			while ((currentLine = br.readLine()) != null) {
				readValues = currentLine.split(";");
			}
			br.close();
		} catch (Exception ex) {
			fail("Cannot read file");
		}
	}
	
}
