package gamestatus;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class SaveGameTest {

	SaveGame saveGame;
	String [] readValues;
	
	@Before
	public void setUp() throws Exception {
		int [][] humanShipsBoard = this.createBoardArray();
		int [][] computerShipsBoard = this.createBoardArray();
		int [][] humansHistory = this.createHistoryArray();
		int [][] computersHistory = this.createHistoryArray();
		int [] humansShips = {2,3,3,4,5};
		int [] computersShips = {2,3,3,4,5};
		boolean isHumansTurn = true;
		saveGame = new SaveGame("J_Unit_Test", humanShipsBoard, 
				computerShipsBoard, humansHistory, computersHistory, 
				isHumansTurn, humansShips, computersShips);
	}

	@Test
	public void testFileCreated() {
		File file = new File("savedGame.txt");
		boolean isFileCreated = false;
		if(file.exists() && !file.isDirectory()) { 
			isFileCreated = true;
		}
		assertTrue(isFileCreated);
	}
	
	@Test
	public void testReadSavedFile() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("savedGame.txt"));
		String currentLine;
		while ((currentLine = br.readLine()) != null) {
			readValues = currentLine.split(";");
		}
		br.close();
		//test that there are 16 values in the file
		assertEquals(16, readValues.length);
		//1st value - players name
		assertEquals("J_Unit_Test", readValues[0].toString());
		//2nd value - who's turn
		assertEquals("humansTurn", readValues[1].toString());
		//array of 100 values when split on ','
		String [] humanBoard = readValues[2].split(",");
		assertEquals(100, humanBoard.length);
		//array of 100 values when split on ','
		String [] computersBoard = readValues[3].split(",");
		assertEquals(100, computersBoard.length);
		//array of 100 values when split on ','
		String [] humanHistory = readValues[4].split(",");
		assertEquals(100, humanHistory.length);
		//array of 100 values when split on ','
		String [] computersHistory = readValues[5].split(",");
		assertEquals(100, computersHistory.length);
	}
	
	private int[][] createBoardArray() {
		int [][] returnArray = new int[10][10];
		for (int x = 0; x < returnArray.length; x++) {
			for (int y = 0; y < returnArray.length; y++) {
				returnArray[x][y] = 0;
			}
		}
		return returnArray;
	}

	private int[][] createHistoryArray() {
		int [][] returnArray = new int[10][10];
		for (int x = 0; x < returnArray.length; x++) {
			for (int y = 0; y < returnArray.length; y++) {
				if (y % 2 == 0) {
					returnArray[x][y] = 9;
				}
			}
		}
		return returnArray;
	}
}
