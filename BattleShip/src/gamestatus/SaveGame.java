package gamestatus;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Saves a game to savedGame.txt in the current directory. Going to need the humans and computers board with 
 * the placed ships.  Then will need all the bomb positions that the human and computer have dropped.  Also
 * need who's turn it was when the game was saved and the players name. Also need how many blocks are left 
 * for each ship of both players.
 * 
 * @author Shaun Alberts
 *
 */

public class SaveGame {

	private int initialHumanArray[][] = new int[10][10];
	private int initialComputerArray[][] = new int[10][10];
	
	private int humansHistoryBombs[][] = new int[10][10];
	private int computerHistoryBombs[][] = new int[10][10];
	
	private String playersName;
	private boolean isHumansTurn;
	
	/**
	 * Takes in all the needed arguments and calls and creates the file.
	 * 
	 * @param playersName
	 * @param humanShipsBoard
	 * @param computerShipsBoard
	 * @param humansHistory
	 * @param computersHistory
	 * @param isHumansTurn
	 * @throws IOException
	 */
	public SaveGame(String playersName, int [][] humanShipsBoard, int [][] computerShipsBoard,
			int [][] humansHistory, int [][] computersHistory, boolean isHumansTurn, 
			int [] humansShipBlock, int [] computersShipBlocks) throws IOException {
		this.playersName = playersName;
		initialHumanArray = humanShipsBoard;
		initialComputerArray = computerShipsBoard;
		humansHistoryBombs = humansHistory;
		computerHistoryBombs = computersHistory;
		if (isHumansTurn = true) {
			this.isHumansTurn = true;
		} else {
			this.isHumansTurn = false;
		}
		this.createFile(humansShipBlock, computersShipBlocks);
	}
	
	/**
	 * Creates the file
	 * 
	 * @throws IOException
	 */
	private void createFile(int[] humansShipBlocksLeft, int [] computersShipBlocksLeft) throws IOException {
		File outFile = new File("savedGame.txt");
		FileWriter fWriter = new FileWriter(outFile);
		PrintWriter pWriter = new PrintWriter(fWriter);
		//players name get written to file first
		pWriter.print(this.getPlayersName() + ';');
		//track who's turn it is
		if (isHumansTurn) {
			pWriter.print("humansTurn;");
		} else {
			pWriter.print("computersTurn;");
		}
		//humans board
		this.writeArrayToFile(initialHumanArray, pWriter);
		//computers board
		this.writeArrayToFile(initialComputerArray, pWriter);
		//humans past dropped bombs
		this.writeArrayToFile(humansHistoryBombs, pWriter);
		//humans past dropped bombs
		this.writeArrayToFile(computerHistoryBombs, pWriter);
		//get the blocks left for each ship
		for (int i = 0; i < humansShipBlocksLeft.length; i++) {
			pWriter.print(humansShipBlocksLeft[i] + ";");
		}
		for (int i = 0; i < computersShipBlocksLeft.length; i++) {
			pWriter.print(computersShipBlocksLeft[i] + ";");
		}
		
		pWriter.close();
	}
	
	/**
	 * Helper method for createFile().  Write a [][] array to file, separated by ','
	 * @param board
	 * @param pWriter
	 */
	private void writeArrayToFile(int [][] board, PrintWriter pWriter) {
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				if (x == 9 && y == 9) {
					pWriter.print(board[x][y]);
				} else {
					pWriter.print(board[x][y] + ",");
				}
			}
		}
		pWriter.print(";");
	}

	/**
	 * Return players name.
	 * @return
	 */
	private String getPlayersName() {
		return playersName;
	}
}
