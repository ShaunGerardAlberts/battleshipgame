package gamestatus;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This is used to load a saved game.  Reads from 'savedGame.txt' and allows all 
 * desired values to be retrieved.
 * 
 * @author Shaun Alberts
 *
 */

public class LoadGame {
	
	private String [] readValues = new String[5];

	public LoadGame() {
		//empty
	}
	
	/**
	 * Opens the file and reads its contents.
	 * 
	 * @return boolean - if file was read or not.
	 * @throws IOException - if file could not be read.
	 */
	public boolean readFile() throws IOException{
		BufferedReader br = new BufferedReader(new FileReader("savedGame.txt"));
			String currentLine;
			while ((currentLine = br.readLine()) != null) {
				readValues = currentLine.split(";");
				return true;
			}
			br.close();
		//get here, successfully then has read file
		return true;
	}
	
	/**
	 * Get the players name.
	 * 
	 * @return String 
	 */
	public String getPlayersName() {
		return readValues[0];
	}
	
	/**
	 * Whos' gets to start.
	 * 
	 * @return boolean - true, human gets to start.
	 */
	public boolean isHumansTurn() {
		if (readValues[1].equals("humansTurn")) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Get the human players array[][].
	 * @return int[][]
	 */
	public int[][] getInitialHumansArray() {
		String humanString= readValues[2];
		return this.convertToTwoDimensional(humanString);
	}
	
	/**
	 * Get the computers array[][].
	 * @return int[][]
	 */
	public int[][] getInitialComputerArray() {
		String computerString = readValues[3];
		return this.convertToTwoDimensional(computerString);
	}
	
	/**
	 * Get the past dropped bombs of the human.
	 * @return int[][]
	 */
	public int[][] getHumansHistoryPicks() {
		String humanHistorySring = readValues[4];
		return this.convertToTwoDimensional(humanHistorySring);
	}
	
	/**
	 * Get the pased dropped bombs of the computer.
	 * @return int[][]
	 */
	public int[][] getComputersHistoryPicks() {
		String computersHistoryString = readValues[5];
		return this.convertToTwoDimensional(computersHistoryString);
	}
	
	/**
	 * Get the number of blocks left for each ship for the human player.
	 * @return int
	 */
	public int getShipOneBlockHuman() {
		return Integer.parseInt(readValues[6]);
	}
	
	/**
	 * Get the number of blocks left for each ship for the human player.
	 * @return int
	 */
	public int getShipTwoBlockHuman() {
		return Integer.parseInt(readValues[7]);
	}
	
	/**
	 * Get the number of blocks left for each ship for the human player.
	 * @return int
	 */
	public int getShipThreeBlockHuman() {
		return Integer.parseInt(readValues[8]);
	}
	
	/**
	 * Get the number of blocks left for each ship for the human player.
	 * @return int
	 */
	public int getShipFourBlockHuman() {
		return Integer.parseInt(readValues[9]);
	}
	
	/**
	 * Get the number of blocks left for each ship for the human player.
	 * @return int
	 */
	public int getShipFiveBlockHuman() {
		return Integer.parseInt(readValues[10]);
	}
	
	/**
	 * now get the number of blocks left for each ship for the computer player
	 * @return int
	 */
	public int getShipOneBlockComputer() {
		return Integer.parseInt(readValues[11]);
	}
	
	/**
	 * now get the number of blocks left for each ship for the computer player
	 * @return int
	 */
	public int getShipTwoBlockComputer() {
		return Integer.parseInt(readValues[12]);
	}
	
	/**
	 * now get the number of blocks left for each ship for the computer player
	 * @return int
	 */
	public int getShipThreeBlockComputer() {
		return Integer.parseInt(readValues[13]);
	}
	
	/**
	 * now get the number of blocks left for each ship for the computer player
	 * @return int
	 */
	public int getShipFourBlockComputer() {
		return Integer.parseInt(readValues[14]);
	}
	
	/**
	 * now get the number of blocks left for each ship for the computer player
	 * @return int
	 */
	public int getShipFiveBlockComputer() {
		return Integer.parseInt(readValues[15]);
	}
	
	/**
	 * Takes a string and convert it to a 2 dimensional int array.
	 * 
	 * Takes a string, converts to a string array, then that string array to int
	 * array. Then that int array to a twoDimensional array
	 * @param oneDimArray
	 * @return two dimensional array
	 */
	private int[][] convertToTwoDimensional(String oneDimArray) {
		//got 100 string values, like this 0,0,3,....,0
		//get int array of 100 values
		int[] intArray = new int[100];
		int[][] twoDimArray = new int[10][10];
		
		//get a string array, will contain 100 values
		String[] stringHumanArray = oneDimArray.split(",");
		//now cast each string[value] to a int and add it to a int array
		for (int i = 0; i < stringHumanArray.length; i++) {
			intArray[i] = Integer.parseInt(stringHumanArray[i]);
		}
		
		//convert to 2 dimensional array, get the array[100] to a array[10][10]
		int count = 0;
		for (int x = 0; x < 10; x++) {
			for (int y = 0; y < 10; y++) {
				if (count == intArray.length) {
					break;
				}
				twoDimArray[x][y] = intArray[count];
	            count++;
			}
		}
		return twoDimArray;
	}

}
