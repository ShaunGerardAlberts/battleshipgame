package simulatedgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import players.PlayerComputer;
import players.PlayerHuman;

import battleshipitems.Constants;

import coorinates.Coordinate;
import coorinates.ListPossibleCoord;

/**
 * This is used to simulate a game. Drops bombs for the human player.
 *  
 * @author Shaun ALberts
 *
 */
public class SimulatedGame {

	PlayerComputer computer;
	PlayerHuman human;
	Random random;
	int countHumanTurns = 0;
	int countComputerTurns = 0;
	private boolean humansTurn;
	
	/**
	 * Starts the simulated game.
	 */
	public SimulatedGame() {
		// create computer player
		computer = new PlayerComputer();
		// create human player
		human = new PlayerHuman("Simulated Game");
		human.rondomPlacePieces();
		System.out.println("\nYour 5 ships have been placed.  Take a look.");
		this.printBoard(human.getplayersBoard());

		humansTurn = this.randomPlayerStart();

		// getArrayList of coordinates for possible positions to drop bombs
		ArrayList<Coordinate> humansCoordinates = new ListPossibleCoord().getArrayListCoordinates();
		ArrayList<Coordinate> computersCoordinates = new ListPossibleCoord().getArrayListCoordinates();

		// randomize the elements of the lists
		Collections.shuffle(humansCoordinates);
		Collections.shuffle(computersCoordinates);

		this.mainGameLoop(humansCoordinates, computersCoordinates);
		this.gameOverviewMessage();

	}
	
	/**
	 * Main loop of the game.  Cycles between the user and the computer, allowing 
	 * them to drop bombs.
	 * @param humansCoordinates,  computersCoordinates
	 */
	private void mainGameLoop(ArrayList<Coordinate> humansCoordinates, ArrayList<Coordinate> computersCoordinates) {
		boolean gameOnGoing = true;
		Coordinate thisCoordinate = null;
		
		while (gameOnGoing) {
			// So now each player has a board and has placed pieces
			if (humansTurn) {
				countHumanTurns++;
				if (!humansCoordinates.isEmpty()) {
					thisCoordinate = humansCoordinates.get(0);
				}
				
				char dropped = computer.dropOpponentsBomb(thisCoordinate.getXPosition(), thisCoordinate.getYPosition());
				humansTurn = this.humansTurnChanged(dropped);
				boolean hasComputerLost = computer.hasPlayerLost();
				if (hasComputerLost) {
					System.out.println("The game is over.  The human has beaten you.");
					gameOnGoing = false;
				}
				if (!gameOnGoing) {
					break;
				}
				if (!humansCoordinates.isEmpty()) {
					humansCoordinates.remove(0);
				}
				//humansTurn = false;
			}
			if (!humansTurn) {// computer turn
				countComputerTurns++;
				if (!computersCoordinates.isEmpty()) {
					thisCoordinate = computersCoordinates.get(0);
				}
				
				char dropped = human.dropOpponentsBomb(thisCoordinate.getXPosition(), thisCoordinate.getYPosition());
				humansTurn = this.computersTurnChanged(dropped);
				boolean hasComputerLost = human.hasPlayerLost();
				if (hasComputerLost) {
					System.out.println("The game is over.  The computer has beaten you.");
					gameOnGoing = false;
				}
				if (!gameOnGoing) {
					break;
				}
				if (!computersCoordinates.isEmpty()) {
					computersCoordinates.remove(0);
				}
			}
		}
	}
	
	/**
	 * If the bomb has been dropped change turns.
	 * @param droppedBombStatus
	 * @return boolean
	 */
	public boolean humansTurnChanged(char droppedBombStatus) {
		if (droppedBombStatus == Constants.ALREADY_BOMBED) {
			return true;// try again
		} else if (droppedBombStatus == Constants.ILLEGAL_POSITION) {
			return true;
		} else if (droppedBombStatus == Constants.MISSED) {
			return false;
		} else if (droppedBombStatus == Constants.HIT_SHIP) { // was a hit, so see
			// if still have blocks left
			 System.out.println("Human Hits");
			 return false;
		} else if (droppedBombStatus == Constants.HIT_SHIP_AND_SUNK) {
			System.out.println("Human has sunk a ship a ship");
			return false;
		}
		return false;
	}
	
	/**
	 * If the bomb has been dropped change turns.
	 * @param droppedStatus
	 * @return boolean
	 */
	public boolean computersTurnChanged(char droppedStatus) {
		if (droppedStatus == Constants.ALREADY_BOMBED) {
			return false;// try again
		} else if (droppedStatus == Constants.ILLEGAL_POSITION) {
			return false;
		} else if (droppedStatus == Constants.MISSED) {
			return true;
		} else if (droppedStatus == Constants.HIT_SHIP) { // was a hit
			 System.out.println("Computer Hits");
			 return true;
		} else if (droppedStatus == Constants.HIT_SHIP_AND_SUNK) {
			System.out.println("Computer has sunk a ship a ship");
			return true;
		}
		return false;
	}
	
	/**
	 * Display message after game is over
	 */
	private void gameOverviewMessage() {
		System.out.println("Computers Board : Turns = " + countComputerTurns);
		this.printBoard(computer.getplayersBoard());
		System.out.println("Humans Board : Turns = " + countHumanTurns);
		this.printBoard(human.getplayersBoard());
		if (human.hasPlayerLost()) {
			System.out.println("\n!!!! You have lost the game. !!!!");
		} else {
			System.out.println("\n!!!!!  You have won. !!!!!!");
		}
	}
	
	/**
	 * Get who is going to start, randomly chosen
	 * @return boolean
	 */
	private boolean randomPlayerStart() {
		random = new Random();
		if (random.nextInt(2) == 0) {
			System.out.println("Simulated human player gets to start");
			return true;
		} else {
			System.out.println("Computer gets to start.");
			return false;
		}
	}
	
	/**
	 * Print a int[][] in a board fashion.
	 * @param board
	 */
	private void printBoard(int [][] board) {
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				System.out.print(board[x][y]);
			}
			System.out.println("");
		}
	}
}
