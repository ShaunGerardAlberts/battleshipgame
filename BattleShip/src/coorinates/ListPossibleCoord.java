package coorinates;

import java.util.ArrayList;

/**
 * This is going to be a two dimensional array of coordinate class.  Will use this to draw random 
 * coordinates when choosing positions to drop bombs.  Basically I want an arraylist of possible 
 * values from a two dimensional array. For example (0,0) all the way to (9,9).  This means I need 100 
 * coordinates.  I'm going to use this to draw random values for the computer and human.  If I simply 
 * choose random variables it can take extremely long.
 * 
 * This will be used by the computer.  
 * Is also used by the human when playing a simulated game(essentially making the computer play a computer).
 * 
 * @author Shaun Alberts
 *
 */

public class ListPossibleCoord {
	
	private ArrayList<Coordinate> arrayLostCoordinates = new ArrayList<>();
	private Coordinate coordinate;
	
	private int loopCount = 10;
	
	/**
	 * Create a array of coordinates, 100 pairs.
	 */
	public ListPossibleCoord() {
		for (int x = 0; x < loopCount; x++) {
			for (int y = 0; y < loopCount; y++) {
				//create a coordinate 
				coordinate = new Coordinate(x, y);
				//add coordinate to a list
				arrayLostCoordinates.add(coordinate);
			}
		}
	}
	
	/**
	 * Return ArrayList of coordinates. This contain 100 pairs.  Matches the coordinates on a 
	 * battle ship game.
	 * @return ArrayList<Coordinate>
	 */
	public ArrayList<Coordinate> getArrayListCoordinates() {
		return arrayLostCoordinates;
	}
}
