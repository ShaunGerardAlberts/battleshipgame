package coorinates;
/**
 * Check that the ArrayList<Coordinate> is getting created correctly.  
 */
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ListPossibleCoordTest {

	ListPossibleCoord listCoords;
	
	/**
	 * Create a list of coordinates
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		listCoords = new ListPossibleCoord();
	}

	/**
	 * Check that the list was created with the correct number of coordinates
	 */
	@Test
	public void testGetArrayListCoordinates() {
		ArrayList<Coordinate> thisCoordList = listCoords.getArrayListCoordinates();
		int sizeList = thisCoordList.size();
		assertEquals(100, sizeList);
	}
	
	/**
	 * Check that the coordinates are created with valid numbers
	 */
	@Test
	public void testContents() {
		boolean legalValues = true;
		ArrayList<Coordinate> thisCoordList = listCoords.getArrayListCoordinates();
		for (Coordinate coord : thisCoordList) {
			if (coord.getXPosition() < 0 || coord.getYPosition() > 9) {
				legalValues = false;
			}
		}
		assertTrue(legalValues);
	}

}
