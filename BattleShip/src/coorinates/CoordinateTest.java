package coorinates;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CoordinateTest {

	private Coordinate coordinate;
	@Before
	public void setUp() throws Exception {
		coordinate = new Coordinate(1, 2);
	}

	@Test
	public void testGetXPosition() {
		int getX = coordinate.getXPosition();
		assertEquals(1, getX);
	}

	@Test
	public void testSetXPosition() {
		coordinate.setXPosition(5);
		int getX = coordinate.getXPosition();
		assertEquals(5, getX);
	}

	@Test
	public void testGetYPosition() {
		int getY = coordinate.getYPosition();
		assertEquals(2, getY);
	}

	@Test
	public void testSetYPosition() {
		coordinate.setYPosition(7);
		int getY = coordinate.getYPosition();
		assertEquals(7, getY);
	}

}
