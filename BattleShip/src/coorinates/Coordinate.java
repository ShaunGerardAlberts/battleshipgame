package coorinates;
/**
 * This represents a coordinate.  Will have two integers, comprising of x and y.
 * 
 * @author Shaun Alberts
 *
 */
public class Coordinate {
	private int xPosition;
	private int yPosition;
	
	/**
	 * Set the x,y coordinates.
	 * @param xPosition
	 * @param yPosition
	 */
	public Coordinate(int xPosition, int yPosition) {
		this.xPosition = xPosition;
		this.yPosition = yPosition;
	}
	
	/**
	 * Get the xPosition.
	 * Return xPosition
	 * @return int
	 */
	public int getXPosition() {
		return xPosition;
	}
	
	/**
	 * Sets the xPosition.
	 * @param xPosition
	 */
	public void setXPosition(int xPosition) {
		this.xPosition = xPosition;
	}
	
	/**
	 * Returns the yPosition.
	 * @return int
	 */
	public int getYPosition() {
		return yPosition;
	}
	
	/**
	 * Sets the yPosition.
	 * @param yPositoin
	 */
	public void setYPosition(int yPositoin) {
		this.yPosition = yPositoin;
	}
}
