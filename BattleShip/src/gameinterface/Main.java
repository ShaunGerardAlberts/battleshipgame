package gameinterface;

/**
 * This is a simple text based user interface for playing BattleShips
 * 
 * @author Shaun Alberts
 * 
 */

public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		TextUserInterface textInterface = new TextUserInterface();
	}
}
	
	