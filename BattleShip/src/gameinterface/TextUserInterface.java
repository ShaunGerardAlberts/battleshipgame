package gameinterface;
import gamestatus.LoadGame;
import gamestatus.SaveGame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

import coorinates.Coordinate;
import coorinates.ListPossibleCoord;

import players.PlayerComputer;
import players.PlayerHuman;
import simulatedgame.SimulatedGame;
import battleshipitems.Constants;

/**
 * Textual interface where the user get to either simulate, play, or load a game.  
 * The user can also save a game.
 * @author Shaun Alberts
 *
 */

public class TextUserInterface {
	
	private PlayerComputer computer;
	private PlayerHuman human;
	private Random random = new Random();
	private Scanner scan;
	
	private String name;
	private boolean humansTurn;
	private int[][] initialHumanBoard;
	private int[][] initialComputerBoard = new int[10][10];
	
	public TextUserInterface() {
		System.out.println("******   Welcome to BattleShips *********");
		//ask if user wants to load a game, play a game or simulate a game
		String userChoice = this.chooseLoadSavePlay();
		
		if (userChoice.equals("s")) {//simulate
			//I kept this in a separate class as it was too different to try use this mainGameLoop here
			@SuppressWarnings("unused")
			SimulatedGame runSimulatedGame = new SimulatedGame();
			System.exit(0);
		} else if (userChoice.equals("p")) {//play a game
			// create computer player, and randomly place ships
			computer = new PlayerComputer();
			System.out.println("I'll let you look at the computers board.");
			this.printBoard(computer.getplayersBoard());
			//get the initial computers board
			initialComputerBoard = this.getInitailBoardState(computer.getplayersBoard());
			
			//get the human player to enter a name
			name = this.enterPlayersName();
			System.out.format("Hello : %s today you are going to play against the computer!", name);
			// create human player
			human = new PlayerHuman(name);
			//ask user if computer can place ships for them
			String autoPlaceShips = this.automaticallyPlacePieces();
			
			//let human place ships if they want to
			if (autoPlaceShips.equalsIgnoreCase("n")) {
				this.placeShipManually();
			} else {//auto place ships for human
				human.rondomPlacePieces();
			}
			System.out.println("\nYour 5 ships have been placed.");
			initialHumanBoard  = this.getInitailBoardState(human.getplayersBoard());
			//randomly decide who gets to start
			humansTurn = this.randomPlayerStart();
		} else {//load a game
			//LoadGame will read from file and return values casted to correct types
			LoadGame loadedGame = new LoadGame();
			this.readFile(loadedGame);
			initialHumanBoard = loadedGame.getInitialHumansArray();
			initialComputerBoard = loadedGame.getInitialComputerArray();
			String playerName = loadedGame.getPlayersName();
			human = new PlayerHuman(playerName);
			human.setPlayersBoard(initialHumanBoard);
			computer = new PlayerComputer();
			computer.setPlayersBoard(initialComputerBoard);
			//adjust the blocks left for each ship
			this.deductShipBlockLeft(loadedGame);
			
			System.out.println("I'll let you look at the computers board.");
			this.printBoard(computer.getplayersBoard());
			System.out.println("Hello " + playerName);
			humansTurn = loadedGame.isHumansTurn();

			int[][] getHumansDropBombs = loadedGame.getHumansHistoryPicks();
			int[][] getComputersDropBombs = loadedGame.getComputersHistoryPicks();
			
			human.dropBombsFromReload(getComputersDropBombs);
			computer.dropBombsFromReload(getHumansDropBombs);
		}

		ArrayList<Coordinate> computersCoordinates = new ListPossibleCoord().getArrayListCoordinates();
		// randomize the elements of the lists
		Collections.shuffle(computersCoordinates);

		this.mainGameLoop(computersCoordinates);
		
		this.gameOverviewMessage();
	}

	/**
	 * ask if user wants to load a game, play a game or simulate a game	
	 * @return String
	 */
	private String chooseLoadSavePlay() {
		//ask if user wants to load a game, play a game or simulate a game
		scan = new Scanner(System.in);
		String userChoice;
		do {
			System.out.println("To simulate a game press 's', \nTo play again the computer press 'p'" +
					"\nTo load a game press 'l'.");
			userChoice = scan.nextLine();
		} while (!userChoice.equals("s") && !userChoice.equals("p") && !userChoice.equals("l"));
		return userChoice;
	}
	
	/**
	 * When loading a saved game this will deduct the block off all the ships
	 * that has been hit with bombs.
	 * @param LoadGame
	 */
	private void deductShipBlockLeft(LoadGame lg) {
		//get the ship blocks left for the human
		human.setShipOneBlocks(lg.getShipOneBlockHuman());
		human.setShipTwoBlocks(lg.getShipTwoBlockHuman());
		human.setShipThreeBlocks(lg.getShipThreeBlockHuman());
		human.setShipFourBlocks(lg.getShipFourBlockHuman());
		human.setShipFiveBlocks(lg.getShipFiveBlockHuman());
		//get the ship blocks left for the computer
		computer.setShipOneBlocks(lg.getShipOneBlockComputer());
		computer.setShipTwoBlocks(lg.getShipTwoBlockComputer());
		computer.setShipThreeBlocks(lg.getShipThreeBlockComputer());
		computer.setShipFourBlocks(lg.getShipFourBlockComputer());
		computer.setShipFiveBlocks(lg.getShipFiveBlockComputer());
	}
	
	/**
	 * Main loop of the game.  Cycles between the user and the computer, allowing 
	 * them to drop bombs.
	 * @param computersCoords
	 */
	private void mainGameLoop(ArrayList<Coordinate> computersCoords) {
		boolean gameOnGoing = true;
		Coordinate thisCoordinate = null;
		while (gameOnGoing) {
			// So now each player has a board and has placed pieces
			if (humansTurn) {
				//print out the humans board
				System.out.println("Your Board : ");
				this.printBoard(human.getplayersBoard());
				//also show them their past picks
				System.out.println("Your previous picks : ");
				this.getPreviousPicks(computer.getplayersBoard());
				
				//get the human to enter in coordinates, and ask them if they want to save or exit game
				int coordX = this.enterCoordinate(Constants.xCoordinate, humansTurn);
				int coordY = this.enterCoordinate(Constants.yCoordinate, humansTurn);
				
				//now drop the bomb and change to the next players turn
				char dropped = computer.dropOpponentsBomb(coordX, coordY);
				humansTurn = this.humansTurnChanged(dropped, coordX, coordY);
				boolean hasComputerLost = computer.hasPlayerLost();
				if (hasComputerLost) {
					gameOnGoing = false;
				}
				if (!gameOnGoing) {
					break;
				}
			}
			if (!humansTurn) {// computer turn
				if (!computersCoords.isEmpty()) {
					thisCoordinate = computersCoords.get(0);
				}
				char dropped = human.dropOpponentsBomb(thisCoordinate.getXPosition(), thisCoordinate.getYPosition());
				humansTurn = this.computersTurnChanged(dropped);
				boolean hasComputerWon = human.hasPlayerLost();
				if (hasComputerWon) {
					gameOnGoing = false;
				}
				if (!gameOnGoing) {
					break;
				}
				if (!computersCoords.isEmpty()) {
					computersCoords.remove(0);
				}
			}
		}
	}
	
	/**
	 * Helper method for mainGameLoop().
	 * @param droppedStatus
	 * @return boolean
	 */
	private boolean humansTurnChanged(char droppedStatus, int cordX, int cordY) {
		if (droppedStatus == Constants.ALREADY_BOMBED) {
			System.out.println("Already Bombed - Coord(" + cordX + ";" + cordY + ")");
			System.out.println("Please try again");
			return true;//try again
		} else if (droppedStatus == Constants.ILLEGAL_POSITION) {
			System.out.print("Illegal position");
			return false;
		} else if (droppedStatus == Constants.MISSED) {
			System.out.println("You did not hit anything");
			return false;
		} else if (droppedStatus == Constants.HIT_SHIP) { // was a hit, so see if still have blocks left
			 System.out.println("Yay you hit somehting");
			return false;
		} else if (droppedStatus == Constants.HIT_SHIP_AND_SUNK) {
			System.out.println("Amazing you sunk a ship!!");
			return false;
		}
		return false;
	}
	
	/**
	 * Helper method for the mainGameLoop()
	 * @param droppedStatus
	 * @return boolean
	 */
	private boolean computersTurnChanged(char droppedStatus) {
		if (droppedStatus == Constants.ALREADY_BOMBED) {
			return false;
		} else if (droppedStatus == Constants.ILLEGAL_POSITION) {
			System.out.print("Illegal position");
			return true;
		} else if (droppedStatus == Constants.MISSED) {
//			System.out.println("You did not hit anything");
			return true;
		} else if (droppedStatus == Constants.HIT_SHIP) { 
			 System.out.println("Computer hit one of your ships");
			return true;
		} else if (droppedStatus == Constants.HIT_SHIP_AND_SUNK) {
			System.out.println("Computer sunk one of your ships");
			return true;
		}
		return false;
	}
	
	/**
	 * Display message after game is over
	 */
	private void gameOverviewMessage() {
		System.out.println("Computers Board");
		this.printBoard(computer.getplayersBoard());
		System.out.println("Humans Board");
		this.printBoard(human.getplayersBoard());
		if (human.hasPlayerLost()) {
			System.out.println("\n!!!! You have lost the game. !!!!");
		} else {
			System.out.println("\n!!!!!  You have won. !!!!!!");
		}
	}
	
	/**
	 * Let the user place their ships manually
	 */
	private void placeShipManually() {
		System.out.println("Place small ship");
		this.placeShip(Constants.SMALL_SHIP);
		System.out.println("First ship was placed.\nPlace medium ship one");
		this.placeShip(Constants.MEDIUM_SHIP_ONE);
		System.out.println("Second ship was placed.\nPlace medium ship two");
		this.placeShip(Constants.MEDIUM_SHIP_TWO);
		System.out.println("Third ship was placed.\nPlace large ship");
		this.placeShip(Constants.LARGE_SHIP);
		System.out.println("Fourth ship was placed.\nPlace xLarge ship");
		this.placeShip(Constants.X_LARGE_SHIP);
		System.out.println("Fith ship was placed.");
	}
	
	/** 
	 * Helper method for the placeShipManually() method.   
	 * @param defineShip
	 */
	private void placeShip(char shipToPlace) {
		boolean isPlaced;
		do {
			isPlaced = this.getDetailsToPlaceShip(shipToPlace);
				if (!isPlaced) {
					System.out.println("Either out of bounds or a ship is already placed on one" +
							"or the blocks.  Try again.");
				}
			} while (!isPlaced);
	}
	
	/**
	 * get the human player to enter a name
	 * @return enterName
	 */
	private String enterPlayersName() {
		String enterName;
		scan = new Scanner(System.in);
		do {
			System.out.println("Human please enter your name : ");
			enterName = scan.nextLine();
		} while (enterName.equals(""));
		return enterName;
	}
	
	/**
	 * Ask the player if they want the computer to place their ships for them or
	 * do they want to do it themselves.
	 * @return autoPlaceShips
	 */
	private String automaticallyPlacePieces() {
		String autoPlaceShips;
		scan = new Scanner(System.in);
		do {
			System.out.println("\nDo you want all you ships to be randomly placed on the board for " +
								"you? 'y' for yes, 'n' for no.");
			autoPlaceShips = scan.nextLine();
		} while((!autoPlaceShips.equalsIgnoreCase("y")) && (!autoPlaceShips.equalsIgnoreCase("n")));
		return autoPlaceShips;
	}
	
	/**
	 * Get who is going to start, randomly chosen
	 * @return boolean
	 */
	private boolean randomPlayerStart() {
		if (random.nextInt(2) == 0) {
			System.out.format("%s gets to start.", name);
			return true;
		} else {
			System.out.println("Computer gets to start.");
			return false;
		}
	}
	
	/**
	 * Depending on the char value, prompts the user for a coordinate.
	 * Repeats until the player enters a valid coordinate. Also asks the user if they want 
	 * to save or exit on the x coordinate.
	 * 
	 * @param xOrYCoord
	 * @return int
	 */
	private int enterCoordinate(char xOrYCoord, boolean isHumanTurn) {
		int userCoord = -1;
		String userInput;
		boolean validCoord;
		do {
			try {
				validCoord = true;
				@SuppressWarnings("resource")
				Scanner sc = new Scanner(System.in);//need this
				if (xOrYCoord == Constants.xCoordinate) {
					System.out.println("Enter X coordinate : (or 's' to save, or 'x' to quit )");
					userInput = sc.next();
					//if it's 'x' want to quit
					if (userInput.equals("x")) {
						System.out.println("Good Bye");
						System.exit(0);
					} else if (userInput.equals("s")) {//user wants to save game
						//save game
						try {
							@SuppressWarnings("unused")
							SaveGame saveGame = new SaveGame(human.getPlayerName(), initialHumanBoard, initialComputerBoard, 
									this.getPreviousPicksForSaving(computer.getplayersBoard()), 
									this.getPreviousPicksForSaving(human.getplayersBoard()),
									isHumanTurn, human.getPlayerBlocksLeft(), computer.getPlayerBlocksLeft());
						} catch (IOException ioException) {
							System.out.println("Game could not be saved.");
						}
						System.out.println("The game has been saved");
						//successfully saved, can't let the user enter a y-coord now
						validCoord = false;
					} else {//not save or quit, so should be an int
						userCoord = Integer.parseInt(userInput);
						if (userCoord > 9 || userCoord < 0) {
							validCoord = false;
							System.out.println("Must be a number between 0 and 9");
						}
					}
				} else {//
					System.out.println("Enter Y Coordinate : ");
					userCoord = sc.nextInt();
					if (userCoord > 9 || userCoord < 0) {
						validCoord = false;
						System.out.println("Must be a number between 0 and 9");
					}
				}
			} catch (Exception ex) {
				validCoord = false;
				System.out.println("Must be a number between 0 and 9");
			}
		} while (!validCoord);
		return userCoord;
	}
	
	/**
	 * Read the saved file.
	 * @param LoadGame
	 */
	private void readFile(LoadGame lg) {
		try {
			lg.readFile();
		} catch (IOException ioe) {
			System.out.println("File could not be read.  Please ensure that a game is saved before" +
					" trying to load one.");
			System.exit(0);
		}
	}
	
	/**
	 * Prints the array[][] is a 10 by 10 fashion.
	 * @param board
	 */
	private void printBoard(int [][] board) {
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				System.out.print(board[x][y]);
			}
			System.out.println("");
		}
	}
	/**
	 * Takes the computers board and only keeps the 9's(previous bombs)
	 * Changes ship values to 0's.
	 * 
	 * @param board
	 */
	private int[][] getPreviousPicksForSaving(int [][] board) {
		int localBoard[][] = new int[10][10];//must use the new keyword here, or else make a reference to board array
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				if (board[x][y] != 9) {
					localBoard[x][y] = 0;
				} else {
					localBoard[x][y] = 9;
				}
			}
		}
		this.printBoard(localBoard);
		return localBoard;
	}
	
	/**
	 * Takes the computers board and only keeps the 9's(previous bombs)
	 * Changes ship values to 0's.
	 * 
	 * @param board
	 */
	private void getPreviousPicks(int [][] board) {
		int localBoard[][] = new int[10][10];//must use the new keyword here, or else make a reference to board array
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				if (board[x][y] != 9) {
					localBoard[x][y] = 0;
				} else {
					localBoard[x][y] = 9;
				}
			}
		}
		this.printBoard(localBoard);
	}
	
	/**
	 * Get the initial state of the board
	 * @param board
	 * @return int[][]
	 */
	private int[][] getInitailBoardState(int [][] board) {
		int intialBoard[][] = new int[10][10];
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				intialBoard[x][y] = board[x][y];
			}
		}
		return intialBoard;
	}
	
	/**
	 * Places a ship at a certain coordinate.  Return details about why ship 
	 * couldn't be placed if the coordinate was out of bound of there was aleardy a 
	 * ship on the position.
	 * 
	 * @param shipNumber
	 * @return boolean
	 */
	private boolean getDetailsToPlaceShip(char shipNumber) {
		boolean validCoord = true;
		int userCoordX = 0;
		int userCoordY = 0;
		char[] shipDir = null;
		do {
			scan = new Scanner(System.in);
			System.out.println("Enter X coordinate : ");
			try {
				userCoordX = scan.nextInt();
				if (userCoordX > 9 || userCoordX < 0) {
					System.out.println("Must be a number between 0 and 9");
					validCoord = false;
				} else {
					validCoord = true;
				}
			} catch (Exception ex) {
				System.out.println("Must be a number between 0 and 9");
				validCoord = false;
			}
		} while (!validCoord);
		//get here, was valid number between 0 and 9
		do {
			System.out.println("Enter y coordinate : ");
			try {
				scan = new Scanner(System.in);
				userCoordY = scan.nextInt();
				if (userCoordY > 9 || userCoordY < 0) {
					System.out.println("Must be a number between 0 and 9");
					validCoord = false;
				} else {
					validCoord = true;
				}
			} catch (Exception ex) {
				System.out.println("Must be a number between 0 and 9");
				validCoord = false;
			}
		} while (!validCoord);
		//get here, have a legal coordinates, find out if horizontal or vertical
		do {
			System.out.println("Do you want to place the ship vertcally or horizontally? 'v' = vertical, " +
						"'h' = horizontal");
			try {
				scan = new Scanner(System.in);
				shipDir = scan.next().toCharArray();
				if (!(shipDir[0] == 'h') && !(shipDir[0] == 'v')) {
					System.out.println("Please enter a 'h' or a 'v'");
					validCoord = false;
				} else {
					validCoord = true;
				}
			} catch (Exception ex) {
				validCoord = false;
				System.out.println("Please enter a 'h' or a 'v'");
			}
		} while(!validCoord);
		
		boolean isShipPlaced = human.explicitlyPlaceShip(shipNumber, userCoordX, userCoordY, shipDir[0]);
		return isShipPlaced;
	}
}
