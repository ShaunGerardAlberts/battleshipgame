package battleshipitems;
/**
 * Represents a ship item that will be placed on a board.  Each ship will be numbered
 * 1 - 5, that represents the 5 ships that are placed on a board.  A ship must know how many blocks 
 * it is comprised of and how many are left after a bomb has been dropped on it.
 * 
 * @author Shaun Alberts
 *
 */
public class Ship {

	private int shipBlocks;//protected
	final private int shipNumber;//protected, final, once initially set cannot be changed
	private int blocksLeft;
	private boolean isSunk = false;
	
	/**
	 * Create a ship.
	 * @param shipNumber
	 * @param shipBlocks
	 */
	public Ship(int shipNumber, int shipBlocks) {
		this.shipBlocks = shipBlocks;
		this.blocksLeft = shipBlocks;
		this.shipNumber = shipNumber;
	}
	
	/**
	 * Returns the numbers of blocks belonging to a ship.
	 * @return int
	 */
	public int getShipBlocks() {
		return shipBlocks;
	}
	
	/**
	 * Sets the blocks left to a ship.
	 * @param setBlocksLeft
	 */
	public void setShipBlocks(int setBlocksLeft) {
		this.blocksLeft = setBlocksLeft;
	}
	
	/**
	 * Returns the ship number.
	 * @return int
	 */
	public int getShipNumber() {
		return shipNumber;
	}
	
	/**
	 * Returns the blocks left to a ship.
	 * @return int
	 */
	public int getBlocksLeft() {
		return blocksLeft;
	}
	
	/**
	 * Decreases the number of blocks left to a ship by 1.
	 */
	public void decreaseBlocksLeft() {
		blocksLeft--;
	}
	
	/**
	 * User to test if a ship has been sunk.
	 * @return boolean
	 */
	public boolean getIsSunk() {
		if (blocksLeft == 0) {
			isSunk = true;
		}
		return isSunk;
	}
}
