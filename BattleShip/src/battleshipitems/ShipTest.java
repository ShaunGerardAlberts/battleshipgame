/**
 * Test the ship class.
 */
package battleshipitems;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ShipTest {

	Ship ship;
	
	@Before
	public void setUp() {
		//create a ship number 1 with 3 containing blocks 
		ship = new Ship(1, 3);
	}

	/**
	 * Check that it return the number of blocks the 
	 * ship contains correctly
	 */
	@Test
	public void testGetShipBlocks() {
		int shipBlockes = ship.getShipBlocks();
		assertEquals(3, shipBlockes);
	}

	/**
	 * Get the number of the ship
	 */
	@Test
	public void testGetShipNumber() {
		int shipNumber = ship.getShipNumber();
		assertEquals(1, shipNumber);
	}

	/**
	 * Check that the number of block left gets returned
	 */
	@Test
	public void testGetBlocksLeft() {
		int blocksLeft = ship.getBlocksLeft();
		assertEquals(3, blocksLeft);
	}

	/**
	 * Check that decreasing a block works correctly
	 */
	@Test
	public void testDecreaseBlocksLeft() {
		ship.decreaseBlocksLeft();
		int blocksLeft = ship.getBlocksLeft();
		assertEquals(2, blocksLeft);
		//get it to 1 block left
		ship.decreaseBlocksLeft();
		blocksLeft = ship.getBlocksLeft();
		assertEquals(1, blocksLeft);
		
	}

	/**
	 * Check that the ship is returned as sunk correctly
	 */
	@Test
	public void testGetIsSunk() {
		ship.decreaseBlocksLeft();
		ship.decreaseBlocksLeft();
		ship.decreaseBlocksLeft();
		boolean isSunk = ship.getIsSunk();
		assertTrue(isSunk);
	}

}
