package battleshipitems;
/**
 * Constant values for use through out the project.
 * @author Shaun Alberts
 *
 */
public class Constants {

	public static char VERTICAL = 'v';
	public static char HORITONTAL = 'h';
	
	public static int SHIP = 1;
	//used in Board.placeShip method
	public static char ILLEGAL_POSITION = 'i';
	public static char POSITION_TAKEN = 't';
	public static char SHIPED_PLACED = 'p';
	public static char ERROR = 'z';
	//used in the dropBomb
	public static int BOMB_MARKER = 9;
	public static char SMALL_SHIP = 'a';
	public static char MEDIUM_SHIP_ONE = 'b';
	public static char MEDIUM_SHIP_TWO = 'c';
	public static char LARGE_SHIP = 'd';
	public static char X_LARGE_SHIP = 'e';
	public static char MISSED = 'm';
	public static char ALREADY_BOMBED = 'f';
	//used in player.dropOpponentBomb
	public static char HIT_SHIP = 'g';
	public static char HIT_SHIP_AND_SUNK = 'j';
	
	public static char xCoordinate = 'x';
	public static char yCoordinate = 'y';
}
