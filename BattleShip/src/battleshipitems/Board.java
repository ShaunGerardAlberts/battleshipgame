package battleshipitems;
/**
 * Represents a battleship board.  Must be 10 rows by 10 columns.
 * 
 * @author Shaun Alberts
 *
 */
public class Board {
	
	private int boardArray[][] = new int[10][10];
	
	/**
	 * Constructor to intialise object.  Assigns object a name and intialises a new fresh board.
	 */
	public Board() {
		this.setBoardArray();
	}

	/**
	 * Returns the array[][] representing the board.
	 * @return int[][] - bloard
	 */
	public int[][] getBoardArray() {
		return boardArray;
	}
	
	/**
	 * Given an array[][] can change this board array[][] to the supplied one.
	 * @param setArray
	 */
	public void setBoardArray(int[][] setArray) {
		this.boardArray = setArray;
	}
	
	/**
	 * Set a new, clean board
	 */
	private void setBoardArray() {
		//populate the board with open indicators. 0 = open
		for (int x = 0; x < getBoardArray().length; x++) {
			for (int y = 0; y < getBoardArray().length; y++) {
				getBoardArray()[x][y] = 0;
			}
		}
	}

	/**
	 * Get the value at a specific point on the board, 0 - open, else - ship already there
	 * Protected to be able to unit test. Initially private 
	 * @param posX
	 * @param posY
	 * @return int - value at coordinate
	 */
	public int getPositionValue(int posX, int posY) {
		return boardArray[posX][posY];
	}
	
	/**
	 * Set a position.  Protected to be able to unit test. Initially private
	 * @param posX
	 * @param posY
	 * @param shipIdentifier
	 */
	protected void setPosition(int posX, int posY, int shipIdentifier) {
		//boardArray[posX][posY] = Constants.ship;
		boardArray[posX][posY] = shipIdentifier;
	}
	
	/**
	 * place a ship on the board
	 * 
	 * @param posX
	 * @param posY
	 * @param direction
	 * @return successful, block occupied, illegal move
	 */
	public boolean placeShip(int posX, int posY, Ship ship, int direction) {
		int blocks = ship.getShipBlocks();//number of blocks for ship 
		int bblock = ship.getShipBlocks();//int bblock = blocks; //change this later when int blocks becomes a object
		int pposX = posX;
		int pposY = posY;
		
		boolean positionOpen = true;
		
		if (posX > 9 || posY > 9) {
			positionOpen = false;
		}
		
		//get the initial point to place the ship and check if it is a legal position and unoccupied
		if (this.getPositionValue(posX, posY) == 0) {
			//account for horizontal moves, x stays the same, y changes
			if (direction == Constants.HORITONTAL) {
				blocks--;//already checked 1st block, so decrease the blocks still to check
				//now check that the rest of the blocks of the ship can also be placed in legal and unoccupied positions
				while (blocks > 0) {
					posY++;
					//posY may never go over 9. else invalid move
					if (posY > 9) {
						positionOpen = false;
						break;
					}
					//position must be unoccupied
					if (this.getPositionValue(posX, posY) != 0) {//
						positionOpen = false;
						break;
						//return "Ship is already placed here";
					}
					blocks--;
				}
			} else {//vertical move, x is changing, y stays the same
				blocks--;//already checked 1st block, so decrease the blocks still to check
				//now check that the rest of the blocks of the ship can also be placed in legal and unoccupied positions
				while (blocks > 0) {
					posX++;
					//posX may never go over 9. else invalid move
					if (posX > 9) {
						positionOpen = false;
						break;
					}
					//position must be unoccupied
					if (this.getPositionValue(posX, posY) != 0) {//
						positionOpen = false;
						break;
						//return "Ship is already placed here";
					}
					blocks--;
				}
			}
		} else {
			positionOpen = false;
			//return "Ship is already placed here";
		}
		
		//now we know that the ship position is legal, so mark the board positions as occupied
		if (positionOpen) {
			this.setPosition(pposX, pposY, ship.getShipNumber());
			if (direction == Constants.HORITONTAL) {//y changes
				bblock--;
				while (bblock > 0) {
					pposY++;
					this.setPosition(posX, pposY, ship.getShipNumber());
					bblock--;
				}
			} else {//vertical, x changes
				bblock--;
				while (bblock > 0) {
					pposX++;
					this.setPosition(pposX, posY, ship.getShipNumber());
					bblock--;
				}
			}
		}
		return positionOpen;
	}
	
	/**
	 * Drops a bomb on the board.  Return back if the placement was legal
	 * if it hit a open spot, and if it hit a ship which ship it hit.
	 * 
	 * @param posX
	 * @param posY
	 * @return char value depicting what was hit.
	 */
	public char dropBomb(int posX, int posY) {
		//test if valid position
		if (posX > 9 || posY > 9) {
			return Constants.ILLEGAL_POSITION;
		}
		
		//get back what is in the position, open, or ship
		int returnStatus = this.getPositionValue(posX, posY);
		switch (returnStatus) {
			case 0://open, therefore miss
				this.setPosition(posX, posY, Constants.BOMB_MARKER);
				return Constants.MISSED;
			case 1://ship 1 has been hit
				this.setPosition(posX, posY, Constants.BOMB_MARKER);
				return Constants.SMALL_SHIP;
			case 2://ship 2 has been hit
				this.setPosition(posX, posY, Constants.BOMB_MARKER);
				return Constants.MEDIUM_SHIP_ONE;
			case 3://hit
				this.setPosition(posX, posY, Constants.BOMB_MARKER);
				return Constants.MEDIUM_SHIP_TWO;
			case 4://hit
				this.setPosition(posX, posY, Constants.BOMB_MARKER);
				return Constants.LARGE_SHIP;
			case 5://hit
				this.setPosition(posX, posY, Constants.BOMB_MARKER);
				return Constants.X_LARGE_SHIP;
			case 9://already been bombed
				return Constants.ALREADY_BOMBED;
			default://error occurred
				return Constants.ERROR;
		}
	}
	
	/**
	 * Find out if the game has been completed.  When all the ships are sunk the game is done.
	 * @return boolean
	 */
	public boolean isGameOver(int [][] board) {
		//there was 2 ways to do this. Either in the game control step, could test all the ship 
		//pieces to see if they were all sunk.  Or by cycling through the boardArray and testing
		//if there are only open and bombs left. I decided the 2nd way would be better
		boolean gameFinished = true;
		//cycle through the 2 dimensional array
		for (int x = 0; x < board.length; x++) {
			for (int y = 0; y < board.length; y++) {
				//test each item in the array[][], if there are only 0's and 9's left game is over.
				if ((board[x][y] != 0) && (board[x][y] != 9)) {
					//System.out.print(board[x][y]);
					return false;//gameFinished = false;
				}
			}
		}
		return gameFinished;
	}
}
