package battleshipitems;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BoardTest {

	protected Board board;
	
	@Before
	public void setUp() {
		board = new Board();
	}
	
	/**
	 * check that after a board is initialized an array[][] is created 
	 * and all the values are initialized to 0
	 */
	@Test
	public void testSetBoardArray() {
		boolean isSet = false;
		int [][] testBoardArray = board.getBoardArray();
		outerloop:
		for (int x = 0; x < testBoardArray.length; x++) {
			for (int y = 0; y < testBoardArray.length; y++) {
				if (testBoardArray[x][y] == 0) {
					isSet = true;
				} else {
					isSet = false;
					break outerloop;
				}
			}
		}
		assertTrue(isSet);
	}
	
	/**
	 * Test that the board is getting returned correctly
	 */
	@Test
	public void testGetBoard() {
		boolean isGot = false;
		int [][] testBoardArray = board.getBoardArray();
		outerloop:
		for (int x = 0; x < testBoardArray.length; x++) {
			for (int y = 0; y < testBoardArray.length; y++) {
				if (testBoardArray[x][y] >= 0 && testBoardArray[x][y] <= 9) {
					isGot = true;
				} else {
					isGot = false;
					break outerloop;
				}
			}
		}
		assertTrue(isGot);
	}
	
	/**
	 * Test the returning of a specific value in the array[][]
	 */
	@Test
	public void testGetPositionValue() {
		boolean validValue = false;
		int valueAtPos = board.getPositionValue(5, 5);
		if (valueAtPos >= 0 && valueAtPos <=9) {
			validValue = true;
		}
		assertTrue(validValue);
	}
	
	/**
	 * Test that ships are getting place correctly
	 */
	@Test
	public void testPlaceShip() {
		Ship ship = new Ship(1,1);
		board.placeShip(0, 0, ship, Constants.VERTICAL);
		int posOne = board.getPositionValue(0, 0);
		assertEquals(1, posOne);
	}
	
	/**
	 * Test that board positions are getting changed correctly
	 */
	@Test
	public void testSetPosition() {
		board.setPosition(0, 0, 0);
		int posOne = board.getPositionValue(0, 0);
		assertEquals(0, posOne);
	}

	/**
	 * Drop a bomb, then see if the correct item was hit.  
	 */
	@Test
	public void testDropBomb() {
		char status = board.dropBomb(10, 5);
		assertEquals(Constants.ILLEGAL_POSITION, status);
		status = board.dropBomb(1, 1);
		assertEquals(Constants.MISSED, status);
		board.setPosition(1, 1, 1);
		status = board.dropBomb(1, 1);
		assertEquals(Constants.SMALL_SHIP, status);
		board.setPosition(1, 1, 2);
		status = board.dropBomb(1, 1);
		assertEquals(Constants.MEDIUM_SHIP_ONE, status);
		board.setPosition(1, 1, 3);
		status = board.dropBomb(1, 1);
		assertEquals(Constants.MEDIUM_SHIP_TWO, status);
		board.setPosition(1, 1, 4);
		status = board.dropBomb(1, 1);
		assertEquals(Constants.LARGE_SHIP, status);
		board.setPosition(1, 1, 5);
		status = board.dropBomb(1, 1);
		assertEquals(Constants.X_LARGE_SHIP, status);
		board.setPosition(1, 1, 0);
	}
	
	/**
	 * Test if the game is over when all the ships are sunk
	 */
	@Test
	public void testIsGameOver() {
		Ship ship = new Ship(1,1);
		board.placeShip(0, 0, ship, Constants.VERTICAL);
		boolean isOver = board.isGameOver(board.getBoardArray());
		assertFalse(isOver);
		board.dropBomb(0, 0);
		isOver = board.isGameOver(board.getBoardArray());
		assertTrue(isOver);
	}
}
