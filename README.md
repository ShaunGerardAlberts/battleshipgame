### What is this repository for? ###
Battleship game for ICT221.

### How do I get set up? ###

Import the folder into eclipse and the run the Main.java file in the gameinterface package.

### Contribution guidelines ###

Junit test included in package of classes that required tests.